# Data Transformation: Cargo Lambda function with Rust

## Function description
This program is built with Cargo Lambda and Rust. This program designed for processing and aggregating data from [Palmer Penguin](https://allisonhorst.github.io/palmerpenguins/) dataset, where it reads the dataset in a CSV format, takes in a specified numerical value(float type) for filtering the data, and returns the mean of selected variables in the dataset by penguin species. It uses the `serde` crate for serialization and deserialization, suggesting that it handles JSON data. It also has the the ability to handle requests in a serverless environment.

## Details
## File Structure
```plaintext
transform/
├── src/                # Source files
│   ├── main.rs         # Main application file for cargo lambda pipeline
│   └── lib.rs          # Main data transformation function
├── Makefile            # short cut for watch, invoke and deploy
├── README.md           # Program overview and instructions
├── Cargo.toml          # Rust package manifest
├── Cargo.lock          
├── .gitlab-ci.yml
└── .gitignore          # Specifies intentionally untracked files to ignore
```
## Function Usage
This program reads the Penguins Dataset from `PENGUIN_DATA`. It assumes the CSV has headers.
The following describe detailed transformation of the dataset:
1. **Filtering**: It filters the rows where the column `culmen_depth_mm` is greater than some specified filter value that user passed in (i.e. 13.5).
2. **Grouping**: The program then groups the data by the `species` column.
3. **Aggregation**: For each group (species in this case), it calculates the mean of `culmen_length_mm`, `culmen_depth_mm`, `flipper_length_mm`.

**Output**: The output dataset will be a dataframe in shape (3, 4), where it is stored as a JSON object and contains mean of the variables mentioned above after filtering by specific value. An error message will be raised if invalid value is inputted. Below are the columns of resulted dataframe:
- `species`: The name of the penguin species
- `culmen_length_mean`: The mean of `culmen_length_mm` values for each species
- `culmen_depth_mean`: The mean of `culmen_depth_mm` values for each species
- `flipper_length_mean`: The mean of `flipper_length_mm` values for each species

## Deployment
This function has been successfully deployed to AWS Lambda in the following invoke URL: https://rt3v8c7a4k.execute-api.us-east-1.amazonaws.com/test/. The function being deployed to is under the name `tranform`. 
Below the picture indicating the success will be provided:
- AWS Lambda Function overview
![Function overview](image/function-overview.png)
- Test successfully
![test](image/test.png)

